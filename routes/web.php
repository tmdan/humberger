<?php



//Роуты админки
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


//Роуты для Client
Route::group(['namespace' => 'Client'], function () {

    //Главная страница
    Route::get('/', 'IndexController@index')->name('frontend.home');

});
