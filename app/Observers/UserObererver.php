<?php

namespace App\Observers;

use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use mysql_xdevapi\Exception;
use TCG\Voyager\Models\Role;

class UserObererver
{

    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function creating(User $user)
    {

        //Присваиваем по умолчанию роль admin
        {
            $user->role_id=Role::where('name','admin')->firstOrFail()->id;
        }


        if($user->role_id && $user->role->name=="dev" && Auth::user()->role->name!=='dev')
        {
            throw new AuthorizationException("УПС - Создание разработчика невозможно в целях безопасности");
        }

    }




    public function deleting(User $user)
    {

        if($user->role_id && $user->role->name=='dev')
        {
            throw new AuthorizationException("УПС - Удаление разработчика невозможно в целях безопасности");
        }


    }



    public function updating(User $user)
    {


        //Присваиваем по умолчанию роль admin
        {
            $user->role_id=User::find([$user->id])->first()->role_id;
        }


        if($user->role_id && $user->role->name=="dev" && Auth::user()->role->name!=='dev')
        {
            throw new AuthorizationException("УПС - Создание разработчика невозможно в целях безопасности");
        }

    }


    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        //
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
