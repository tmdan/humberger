<?php

namespace App\Widgets;

use App\Category;
use App\Product;
use App\Wallpaper;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class CategoriesWidget extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Category::all()->count();
        $string = trans_choice('категория|категории|категорий', $count);

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-list',
            'title'  => "{$count} {$string}",
            'text'   => __('voyager::dimmer.user_text', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => "Все категории",
                'link' => route('voyager.categories.index'),
            ],
            'image' => asset('storage/widgets/categories.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        //return app('VoyagerAuth')->user()->can('browse', Voyager::model('Product'));
        return true;
    }
}
