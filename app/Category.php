<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";

    public $timestamps = false;

    protected $fillable = [
        'title',
        'order',
        'icon',
        'slug',
    ];

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
