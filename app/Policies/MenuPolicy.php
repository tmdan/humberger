<?php

namespace App\Policies;

use App\User;
use App\Menu;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Policies\BasePolicy;

class MenuPolicy extends BasePolicy
{
    use HandlesAuthorization;



    /**
     * Determine whether the user can view the menu.
     *
     * @param  \App\User  $user
     * @param  \App\Menu  $menu
     * @return mixed
     */
    public function index(Request $request)
    {
        return true;
    }


    /**
     * Check if user has an associated permission.
     *
     * @param \TCG\Voyager\Contracts\User $user
     * @param object                      $model
     * @param string                      $action
     *
     * @return bool
     */
    protected function checkPermission(\TCG\Voyager\Contracts\User $user, $model, $action)
    {



        if (!isset(self::$datatypes[get_class($model)])) {
            $dataType = Voyager::model('DataType');
            self::$datatypes[get_class($model)] = $dataType->where('model_name', get_class($model))->first();
        }


        if($model instanceof \TCG\Voyager\Models\Menu && request()->route()->menu==1 && $user->role->name!="dev")
        {
            return false;
        }


        $dataType = self::$datatypes[get_class($model)];

        return $user->hasPermission($action.'_'.$dataType->name);
    }

}
