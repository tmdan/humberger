<?php

namespace App\Policies;

use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;


    public function editRoles(\TCG\Voyager\Contracts\User $user, $model)
    {
        // Does this record belong to another user?
        $another = $user->id != $model->id;

        return $another && $user->hasPermission('edit_roles');
    }

    public function browse(User $user){

        return $user->hasPermission('browse_users');
    }



    public function read(User $user, User $model)
    {
//        if($model->email==env("SUPER_ADMIN_EMAIL"))
//            return false;

        return $user->hasPermission('read_users');
    }



    public function add(User $user)
    {
        return $user->hasPermission('add_users');
    }


    public function edit(User $user, User $model)
    {
        if($model->email==env("SUPER_ADMIN_EMAIL") &&$user->role->name!='dev')
            return false;


       return $user->hasPermission('edit_users');
    }



    public function delete(User $user, User $model)
    {
        if($model->email==env("SUPER_ADMIN_EMAIL"))
            return false;

        return $user->hasPermission('delete_users');
    }

}
