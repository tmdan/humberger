<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use TCG\Voyager\Facades\Voyager;

class ProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'image' =>Voyager::image($this->image),
            'description'=>$this->description,
            'price'=>$this->price,
            'order'=>$this->order,
            'on_sale'=>$this->on_sale,
            'status'=>$this->status,
        ];
    }
}
