<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoriesResources extends ResourceCollection
{

    public function toArray($request)
    {
        return  CategoriesResource::collection($this->collection);
    }
}
