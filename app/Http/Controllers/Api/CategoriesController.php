<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Http\Resources\CategoriesResources;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories=Category::with(["products" => function($q){
            $q->where('products.status', true);
        }])->get();


        return new CategoriesResources($categories);
    }
}
