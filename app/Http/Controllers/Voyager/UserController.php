<?php

namespace App\Http\Controllers\Voyager;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class UserController extends VoyagerBaseController
{


    public function update(Request $request, $id)
    {

        if(User::find([$id])->first()->email==env("ADMIN_EMAIL"))
        {

            $request->validate([
                'email' => 'email|max:255',
                'name' => 'required|string',
            ]);


        }else{

            $request->validate([
                'email' => 'email|required|max:255',
                'name' => 'required|string',
            ]);
        }

        return parent::update($request, $id);
    }

    

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'email|required|unique:users|max:255',
            'name' => 'required|string|max:255',
            'password' => 'required',
        ]);

        return parent::store($request);
    }

}
