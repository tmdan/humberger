<?php

namespace App\Http\Controllers\Client;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $categories=Category::with(["products" => function($q){
            $q->where('products.status', true);
        }])->get();

        $products=Product::where('status', true);

        $productsOnSale=$products->where('on_sale', true)->get();

        $bg=["settings\site\bg1.jpg", "settings\site\bg2.jpg", "settings\site\bg3.jpg"];
        $rand=array_rand([1,2,3],1);
        $bg=$bg[$rand];

        return view('client.index', compact('categories', 'products', 'productsOnSale', 'bg'));
    }
}
