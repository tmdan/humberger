<?php

namespace App\Providers;

use App\Feedback;
use App\FeedbackEmail;
use App\Observers\FeedbackObserver;
use App\Observers\UserObererver;
use App\User;
use Illuminate\Support\ServiceProvider;

class EloquentEventServiceProvider extends ServiceProvider
{

    public function register()
    {
        //
    }





    public function boot()
    {
        User::observe(UserObererver::class);

    }
}
