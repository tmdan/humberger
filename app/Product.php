<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";

    protected $fillable = [
        'title',
        'image',
        'description',
        'price',
        'order',
        'slug',
        'on_sale',
        'status',
    ];


    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }
}
