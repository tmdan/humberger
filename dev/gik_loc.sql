-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 22 2019 г., 19:11
-- Версия сервера: 5.7.20-log
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `gik.loc`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `title`, `icon`, `order`, `slug`) VALUES
(1, 'Бургеры', 'fa-align-center', 1, 'burgers'),
(2, 'Напитки', 'fa-bitbucket', 2, 'drinks'),
(3, 'Фри', 'fa-bolt', 2, 'fries');

-- --------------------------------------------------------

--
-- Структура таблицы `category_product`
--

CREATE TABLE `category_product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `category_product`
--

INSERT INTO `category_product` (`id`, `category_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-07-22 16:10:58', '2019-07-22 16:10:58'),
(2, 1, 2, '2019-07-22 16:10:58', '2019-07-22 16:10:58'),
(3, 1, 3, '2019-07-22 16:10:58', '2019-07-22 16:10:58'),
(4, 1, 4, '2019-07-22 16:10:58', '2019-07-22 16:10:58'),
(5, 2, 5, '2019-07-22 16:10:58', '2019-07-22 16:10:58'),
(6, 2, 6, '2019-07-22 16:10:58', '2019-07-22 16:10:58'),
(7, 2, 7, '2019-07-22 16:10:58', '2019-07-22 16:10:58'),
(8, 2, 8, '2019-07-22 16:10:58', '2019-07-22 16:10:58'),
(9, 3, 9, '2019-07-22 16:10:58', '2019-07-22 16:10:58'),
(10, 3, 10, '2019-07-22 16:10:58', '2019-07-22 16:10:58'),
(11, 3, 11, '2019-07-22 16:10:58', '2019-07-22 16:10:58'),
(12, 1, 11, '2019-07-22 16:10:58', '2019-07-22 16:10:58');

-- --------------------------------------------------------

--
-- Структура таблицы `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Имя', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|email\"}}', 3),
(4, 1, 'password', 'password', 'Пароль', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Дата регистрации', 0, 0, 0, 0, 0, 0, NULL, 6),
(7, 1, 'email_verified_at', 'timestamp', 'voyager::seeders.data_rows.email_verified_at', 0, 0, 0, 0, 0, 0, NULL, 6),
(8, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(9, 1, 'avatar', 'image', 'Аватар', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"image\"}}', 8),
(10, 1, 'user_belongsto_role_relationship', 'relationship', 'Роль', 1, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(11, 1, 'role_id', 'number', 'Идентификатор Роли', 0, 1, 1, 1, 1, 1, NULL, 4),
(12, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 0, 0, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(13, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(14, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(15, 4, 'title', 'text', 'Название', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|string|min:2|max:50\"}}', 2),
(16, 4, 'icon', 'text', 'Иконка', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 1),
(17, 4, 'order', 'number', 'Порядок', 0, 0, 0, 0, 0, 0, NULL, 3),
(18, 4, 'slug', 'text', 'ЧПУ', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:categories,slug\"}}', 6),
(19, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(20, 5, 'title', 'text', 'Название', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"string|min:2|max:50\"}}', 2),
(21, 5, 'image', 'image', 'Изображение', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"image\"},\"resize\":{\"width\":\"250\",\"height\":\"250\"},\"quality\":\"80%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"350\",\"height\":\"350\"}}]}', 3),
(22, 5, 'order', 'number', 'Порядок', 0, 0, 0, 0, 0, 0, NULL, 3),
(23, 5, 'price', 'number', 'Цена', 0, 0, 1, 1, 1, 0, '{\"validation\":{\"rule\":\"required|min:2\"}}', 3),
(24, 5, 'on_sale', 'checkbox', 'На скидке', 0, 1, 1, 1, 1, 1, '{\"on\":\"\\u0421\\u043a\\u0438\\u0434\\u043a\\u0430\",\"off\":\"\\u041e\\u0431\\u044b\\u0447\\u043d\\u0430\\u044f\"}', 3),
(25, 5, 'status', 'checkbox', 'Статус', 0, 1, 1, 1, 1, 1, '{\"on\":\"\\u0410\\u043a\\u0442\\u0438\\u0432\\u043d\\u044b\\u0439\",\"off\":\"\\u041d\\u0435 \\u0430\\u043a\\u0442\\u0438\\u0432\\u043d\\u044b\\u0439\"}', 4),
(26, 5, 'description', 'rich_text_box', 'Описание', 1, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|min:10|max:1000\"}}', 5),
(27, 5, 'slug', 'text', 'ЧПУ', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:categories,slug\"}}', 2),
(28, 5, 'product_belongstomany_category_relationship', 'relationship', 'Категории', 1, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Category\",\"table\":\"categories\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"category_product\",\"pivot\":\"1\",\"taggable\":\"on\"}', 4),
(29, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(31, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(32, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'пользователя', 'Пользователи', 'voyager-person', 'App\\User', 'App\\Policies\\UserPolicy', 'App\\Http\\Controllers\\Voyager\\UserController', '', 1, 0, NULL, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list-add', 'TCG\\Voyager\\Models\\Menu', '\\App\\Policies\\MenuPolicy', '\\App\\Http\\Controllers\\Voyager\\MenuController', '', 1, 0, NULL, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(4, 'categories', 'categories', 'категорию', 'Категории', 'voyager-list', 'App\\Category', NULL, NULL, NULL, 1, 0, NULL, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(5, 'products', 'products', 'товар', 'Товары', 'voyager-hotdog', 'App\\Product', NULL, NULL, NULL, 1, 0, NULL, '2019-07-22 13:10:58', '2019-07-22 13:10:58');

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(2, 'client', '2019-07-22 13:10:58', '2019-07-22 13:10:58');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', '#F4A460', NULL, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58', 'voyager.dashboard', NULL),
(2, 1, 'Users', '', '_self', 'voyager-person', '#DC143C', NULL, 3, '2019-07-22 13:10:58', '2019-07-22 13:10:58', 'voyager.users.index', NULL),
(3, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2019-07-22 13:10:58', '2019-07-22 13:10:58', 'voyager.roles.index', NULL),
(4, 1, 'Tools', '', '_self', 'voyager-tools', '#32CD32', NULL, 9, '2019-07-22 13:10:58', '2019-07-22 13:10:58', NULL, NULL),
(5, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 4, 10, '2019-07-22 13:10:58', '2019-07-22 13:10:58', 'voyager.menus.index', NULL),
(6, 1, 'Database', '', '_self', 'voyager-data', NULL, 4, 11, '2019-07-22 13:10:58', '2019-07-22 13:10:58', 'voyager.database.index', NULL),
(7, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 4, 12, '2019-07-22 13:10:58', '2019-07-22 13:10:58', 'voyager.compass.index', NULL),
(8, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 4, 13, '2019-07-22 13:10:58', '2019-07-22 13:10:58', 'voyager.bread.index', NULL),
(9, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2019-07-22 13:10:58', '2019-07-22 13:10:58', 'voyager.settings.index', NULL),
(10, 1, 'Категории', '', '_self', 'voyager-list', '#1E90FF', NULL, 15, '2019-07-22 13:10:58', '2019-07-22 13:10:58', 'voyager.categories.index', NULL),
(11, 1, 'Товары', '', '_self', 'voyager-hotdog', '#FA8072', NULL, 15, '2019-07-22 13:10:58', '2019-07-22 13:10:58', 'voyager.products.index', NULL),
(12, 2, 'Главная', '', '_self', 'voyager-boat', '#F4A460', NULL, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58', NULL, NULL),
(13, 2, 'О нас', '/#about', '_self', 'voyager-boat', '#F4A460', NULL, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58', NULL, NULL),
(14, 2, 'Меню', '/#menu', '_self', 'voyager-boat', '#F4A460', NULL, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58', NULL, NULL),
(15, 2, 'Скидки', '/#sale', '_self', 'voyager-boat', '#F4A460', NULL, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(26, '2014_10_12_000000_create_users_table', 1),
(27, '2014_10_12_100000_create_password_resets_table', 1),
(28, '2016_01_01_000000_add_voyager_user_fields', 1),
(29, '2016_01_01_000000_create_data_types_table', 1),
(30, '2016_05_19_173453_create_menu_table', 1),
(31, '2016_10_21_190000_create_roles_table', 1),
(32, '2016_10_21_190000_create_settings_table', 1),
(33, '2016_11_30_135954_create_permission_table', 1),
(34, '2016_11_30_141208_create_permission_role_table', 1),
(35, '2016_12_26_201236_data_types__add__server_side', 1),
(36, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(37, '2017_01_14_005015_create_translations_table', 1),
(38, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(39, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(40, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(41, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(42, '2017_08_05_000000_add_group_to_settings_table', 1),
(43, '2017_11_26_013050_add_user_role_relationship', 1),
(44, '2017_11_26_015000_create_user_roles_table', 1),
(45, '2018_03_11_000000_add_user_settings', 1),
(46, '2018_03_14_000000_add_details_to_data_types_table', 1),
(47, '2018_03_16_000000_make_settings_value_nullable', 1),
(48, '2019_07_21_222450_create_products_table', 1),
(49, '2019_07_21_222451_create_categories_table', 1),
(50, '2019_07_21_222451_create_category_product_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(2, 'browse_bread', NULL, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(3, 'browse_database', NULL, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(4, 'browse_media', NULL, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(5, 'browse_compass', NULL, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(6, 'browse_menus', 'menus', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(7, 'read_menus', 'menus', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(8, 'edit_menus', 'menus', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(9, 'add_menus', 'menus', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(10, 'delete_menus', 'menus', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(11, 'browse_roles', 'roles', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(12, 'read_roles', 'roles', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(13, 'edit_roles', 'roles', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(14, 'add_roles', 'roles', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(15, 'delete_roles', 'roles', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(16, 'browse_users', 'users', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(17, 'read_users', 'users', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(18, 'edit_users', 'users', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(19, 'add_users', 'users', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(20, 'delete_users', 'users', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(21, 'browse_settings', 'settings', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(22, 'read_settings', 'settings', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(23, 'edit_settings', 'settings', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(24, 'add_settings', 'settings', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(25, 'delete_settings', 'settings', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(26, 'browse_dev', 'dev', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(27, 'read_dev', 'dev', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(28, 'edit_dev', 'dev', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(29, 'add_dev', 'dev', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(30, 'delete_dev', 'dev', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(31, 'browse_categories', 'categories', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(32, 'read_categories', 'categories', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(33, 'edit_categories', 'categories', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(34, 'add_categories', 'categories', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(35, 'delete_categories', 'categories', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(36, 'browse_products', 'products', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(37, 'read_products', 'products', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(38, 'edit_products', 'products', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(39, 'add_products', 'products', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(40, 'delete_products', 'products', '2019-07-22 13:10:58', '2019-07-22 13:10:58');

-- --------------------------------------------------------

--
-- Структура таблицы `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 2),
(3, 2),
(4, 1),
(4, 2),
(4, 3),
(5, 2),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3),
(8, 1),
(8, 2),
(9, 1),
(9, 2),
(10, 1),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 1),
(16, 2),
(16, 3),
(17, 1),
(17, 2),
(17, 3),
(18, 1),
(18, 2),
(19, 1),
(19, 2),
(20, 1),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(25, 2),
(26, 2),
(27, 2),
(28, 2),
(29, 2),
(30, 2),
(31, 1),
(31, 2),
(31, 3),
(32, 1),
(32, 2),
(32, 3),
(33, 1),
(33, 2),
(34, 1),
(34, 2),
(35, 1),
(35, 2),
(36, 1),
(36, 2),
(36, 3),
(37, 1),
(37, 2),
(37, 3),
(38, 1),
(38, 2),
(39, 1),
(39, 2),
(40, 1),
(40, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `on_sale` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `title`, `image`, `slug`, `description`, `price`, `order`, `on_sale`, `status`, `created_at`, `updated_at`) VALUES
(1, 'BACON BURGER', 'products/example/5.jpg', 'bacon_burger', 'Га́мбургер — вид сэндвича, состоящий из рубленой жареной котлеты, подаваемой внутри разрезанной булки. В дополнение к мясу гамбургер может иметь большое количество разнообразных наполнителей, например: кетчуп и майонез, дольку кабачка, листья салата (Lactuca sativa), маринованный огурец, сырой или жареный лук, помидор.', 2.50, 1, 0, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(2, 'CHEESE SANDWICH', 'products/example/6.jpg', 'cheese_sandwich', 'Га́мбургер — вид сэндвича, состоящий из рубленой жареной котлеты, подаваемой внутри разрезанной булки. В дополнение к мясу гамбургер может иметь большое количество разнообразных наполнителей, например: кетчуп и майонез, дольку кабачка, листья салата (Lactuca sativa), маринованный огурец, сырой или жареный лук, помидор.', 2.70, 2, 1, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(3, 'DELICIOUS SANDWICH', 'products/example/7.jpg', 'delicious_sandwich', 'Га́мбургер — вид сэндвича, состоящий из рубленой жареной котлеты, подаваемой внутри разрезанной булки. В дополнение к мясу гамбургер может иметь большое количество разнообразных наполнителей, например: кетчуп и майонез, дольку кабачка, листья салата (Lactuca sativa), маринованный огурец, сырой или жареный лук, помидор.', 2.90, 3, 0, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(4, 'MORNING SANDWICH', 'products/example/1.png', 'morning_sandwich', 'Га́мбургер — вид сэндвича, состоящий из рубленой жареной котлеты, подаваемой внутри разрезанной булки. В дополнение к мясу гамбургер может иметь большое количество разнообразных наполнителей, например: кетчуп и майонез, дольку кабачка, листья салата (Lactuca sativa), маринованный огурец, сырой или жареный лук, помидор.', 2.30, 4, 1, 0, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(5, 'COLA BOTTLE', 'products/example/cola_bottle.jpg', 'cola_bottle', 'Напиток кола — тип газированных сладких напитков, зачастую содержащих кофеин. Название происходит от орешков кола, изначально использовавшихся производителями напитков в качестве источника кофеина.', 1.30, 5, 1, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(6, 'FRESH LIME', 'products/example/fresh_lime.png', 'fresh_lime', 'Напиток безалкогольный сокосодержащий негазированный Вельта-Пенза Fresh Lime - отзывы', 1.30, 5, 0, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(7, 'Cold Tea', 'products/example/cold_tea.jpg', 'cold_tea', 'Холодный чай — один из видов сервировки чая. Как популярный прохладительный напиток, холодный чай получил широкое распространение в России, странах СНГ и Балтии относительно недавно, после 2000 года. ', 1.40, 6, 0, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(8, 'Black Coffee', 'products/example/black_coffee.jpg', 'black_coffee', 'Блэк латте – кофейный концентрат, который достаточно агрессивно рекламируют как универсальное средство для похудения. Чтобы разобраться в свойствах напитка, внимательно изучим его состав, историю появления и опыт применения на практике.', 1.45, 7, 0, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(9, 'PENE SALMONE', 'products/example/8.jpg', 'pine_salmone', 'Блэк латте – кофейный концентрат, который достаточно агрессивно рекламируют как универсальное средство для похудения. Чтобы разобраться в свойствах напитка, внимательно изучим его состав, историю появления и опыт применения на практике.', 3.45, 8, 1, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(10, 'CHEESE BUTTER', 'products/example/9.jpg', 'cheese_butter', 'Блэк латте – кофейный концентрат, который достаточно агрессивно рекламируют как универсальное средство для похудения. Чтобы разобраться в свойствах напитка, внимательно изучим его состав, историю появления и опыт применения на практике.', 1.00, 9, 0, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(11, 'GENERAL BURGER', 'products/example/general.jpg', 'general_burger', 'Блэк латте – кофейный концентрат, который достаточно агрессивно рекламируют как универсальное средство для похудения. Чтобы разобраться в свойствах напитка, внимательно изучим его состав, историю появления и опыт применения на практике.', 6.00, 10, 1, 1, '2019-07-22 13:10:58', '2019-07-22 13:10:58');

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Админ', '2019-07-22 13:10:57', '2019-07-22 13:10:57'),
(2, 'dev', 'Разработчик', '2019-07-22 13:10:57', '2019-07-22 13:10:57'),
(3, 'guest', 'Гость', '2019-07-22 13:10:57', '2019-07-22 13:10:57');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Название сайта', 'HAMBURGER', '', 'text', 1, 'Site'),
(2, 'site.description', 'Описание сайта', 'Описание сайта', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Логотип', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'site.bg_image', 'Admin Background Image', 'settings\\site\\bg.jpg', '', 'image', 5, 'Site'),
(6, 'admin.bg_image', 'Admin Background Image', 'settings\\admin\\bg.png', '', 'image', 5, 'Admin'),
(7, 'admin.title', 'Admin Title', 'HAMBURGER', '', 'text', 1, 'Admin'),
(8, 'admin.description', 'Admin Description', 'HAMBURGER - Integer pulvinar leo id viverra feugiat. Pellentesque Libero Justo, Semper At Tempus Vel, Ultrices In Sed Ligula. Nulla Uter Sollicitudin Velit.', '', 'text', 2, 'Admin'),
(9, 'admin.loader', 'Admin Loader', 'settings\\admin\\loader.png', '', 'image', 3, 'Admin'),
(10, 'admin.icon_image', 'Admin Icon Image', 'settings\\admin\\logo.png', '', 'image', 4, 'Admin'),
(11, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Структура таблицы `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'menu_items', 'title', 1, 'ru', 'Панель', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(2, 'menu_items', 'title', 2, 'ru', 'Пользователи', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(3, 'menu_items', 'title', 3, 'ru', 'Роли', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(4, 'menu_items', 'title', 4, 'ru', 'Инструменты', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(5, 'menu_items', 'title', 5, 'ru', 'Меню', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(6, 'menu_items', 'title', 6, 'ru', 'Базаданных', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(7, 'menu_items', 'title', 7, 'ru', 'Компас', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(8, 'menu_items', 'title', 8, 'ru', 'Хлебные крошки', '2019-07-22 13:10:58', '2019-07-22 13:10:58'),
(9, 'menu_items', 'title', 9, 'ru', 'Настройки', '2019-07-22 13:10:58', '2019-07-22 13:10:58');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 2, 'Developer', 'https://t.me/tmdan', 'users/dev.png', NULL, '$2y$10$KlfcU/YdBxiRnyYh6i1VFOR7y9umDpP6NUJFcTeG4/HDRYGkRZlX6', 'IqCwH8ySzdl14m3dxKYxbbyKq16DKiS77ct0fhqXLijx08b6Ql1uE7LWoSX8', '{\"locale\":\"ru\"}', '2019-07-22 13:10:57', '2019-07-22 13:10:57'),
(2, 1, 'Admin', 'admin', 'users/admin.png', NULL, '$2y$10$mbii4oFqNE0FSwmWePQwgOC0E1HOr0RBzsHwZpZ2E77rsU5CSNuai', '3d3Bk4um7TCSYLvl45KRjGEYPAWpJelrgIkNoENRbmDni8FaKt8vlIBk2biT', '{\"locale\":\"ru\"}', '2019-07-22 13:10:57', '2019-07-22 13:10:57'),
(3, 3, 'Гость', 'test', 'users/admin.png', NULL, '$2y$10$Sif3MUzI9AEgy/r5zaSq2ODSjQ71Vv8fFFgi4bIjMgpVwAoQM7b..', 'Pr6GiaG43nWYWrTicCMsIc0zSSGSbNAryyiNRYlA5Tms89aNA66LeNE77lpl', '{\"locale\":\"ru\"}', '2019-07-22 13:10:58', '2019-07-22 13:10:58');

-- --------------------------------------------------------

--
-- Структура таблицы `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Индексы таблицы `category_product`
--
ALTER TABLE `category_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_product_category_id_foreign` (`category_id`),
  ADD KEY `category_product_product_id_foreign` (`product_id`);

--
-- Индексы таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Индексы таблицы `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Индексы таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Индексы таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Индексы таблицы `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `category_product`
--
ALTER TABLE `category_product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT для таблицы `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `category_product`
--
ALTER TABLE `category_product`
  ADD CONSTRAINT `category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
