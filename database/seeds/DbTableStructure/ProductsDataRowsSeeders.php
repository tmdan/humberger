<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class ProductsDataRowsSeeders extends DataRowsSeeder
{


    public function run()
    {
        $DataType = DataType::where('slug', 'products')->firstOrFail();


        $dataRow = $this->dataRow($DataType, 'id');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => __('voyager::seeders.data_rows.id'),
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 1,
            ])->save();
        }



        $dataRow = $this->dataRow($DataType, 'title');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => "Название",
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 2,
                'details'=>[
                    'validation' => [
                        'rule' => 'string|min:2|max:50',
                    ],
                ]
            ])->save();
        }


        $dataRow = $this->dataRow($DataType, 'image');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'image',
                'display_name' => "Изображение",
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 3,
                'details' => [
                    "validation"=>[
                      "rule"=>"image",
                    ],
                    'resize' =>
                        [
                            'width' => '250',
                            'height' => '250',
                        ],
                    'quality' => '80%',
                    'upsize' => true,
                    'thumbnails' =>
                        [
                            [
                                'name' => 'medium',
                                'scale' => '50%',
                            ],
                            [
                                'name' => 'small',
                                'scale' => '25%',
                            ],
                            [
                                'name' => 'cropped',
                                'crop' =>
                                    [
                                        'width' => '350',
                                        'height' => '350',
                                    ],
                            ],
                        ],
                ],
            ])->save();
        }


        $dataRow = $this->dataRow($DataType, 'order');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => "Порядок",
                'required'     => 0,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 3,
            ])->save();
        }



        $dataRow = $this->dataRow($DataType, 'price');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => "Цена",
                'required'     => 0,
                'browse'       => 0,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 0,
                'order'        => 3,
                'details'=>[
                    'validation' => [
                        'rule' => 'required|min:2',
                    ],
                ]
            ])->save();
        }




        $dataRow = $this->dataRow($DataType, 'on_sale');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'checkbox',
                'display_name' => "На скидке",
                'required'     => 0,
                'read'         => 1,
                'edit'         => 1,
                'browse'       => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 3,
                'details'      => [
                    "on"=>"Скидка",
                    "off"=>"Обычная"
                ],
            ])->save();
        }



        $dataRow = $this->dataRow($DataType, 'status');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'checkbox',
                'display_name' => "Статус",
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 4,
                'details'      => [
                    "on"=>"Активный",
                    "off"=>"Не активный"
                ],
            ])->save();
        }



        $dataRow = $this->dataRow($DataType, 'description');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'rich_text_box',
                'display_name' => "Описание",
                'required'     => 1,
                'browse'       => 0,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 5,
                'details'=>[
                    'validation' => [
                        'rule' => 'required|min:10|max:1000',
                    ],
                ]
            ])->save();
        }



        $dataRow = $this->dataRow($DataType, 'slug');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => "ЧПУ",
                'required'     => 1,
                'browse'       => 0,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    'slugify' => [
                        'origin' => 'title',
                    ],
                    'validation' => [
                        'rule'  => 'unique:categories,slug',
                    ],
                ],
                'order' => 2,
            ])->save();
        }




        $dataRow = $this->dataRow($DataType, 'product_belongstomany_category_relationship');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'relationship',
                'display_name' => "Категории",
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    "model"=>"App\\Category",
                    "table"=>"categories",
                    "type"=>"belongsToMany",
                    "column"=>"id",
                    "key"=>"id",
                    "label"=>"title",
                    "pivot_table"=>"category_product",
                    "pivot"=>"1",
                    "taggable"=>"on"
                ],
                'order' => 4,
            ])->save();
        }




    }
}
