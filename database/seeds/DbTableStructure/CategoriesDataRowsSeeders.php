<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class CategoriesDataRowsSeeders extends DataRowsSeeder
{


    public function run()
    {
        $DataType = DataType::where('slug', 'categories')->firstOrFail();


        $dataRow = $this->dataRow($DataType, 'id');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => __('voyager::seeders.data_rows.id'),
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 1,
            ])->save();
        }



        $dataRow = $this->dataRow($DataType, 'title');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => "Название",
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 2,
                'details'=>[
                    'validation' => [
                        'rule' => 'required|string|min:2|max:50',
                    ],
                ]
            ])->save();
        }



        $dataRow = $this->dataRow($DataType, 'icon');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => "Иконка",
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 1,
                'details'=>[
                    'validation' => [
                        'rule' => 'required',
                    ],
                ]
            ])->save();
        }



        $dataRow = $this->dataRow($DataType, 'order');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => "Порядок",
                'required'     => 0,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 3,
            ])->save();
        }



        $dataRow = $this->dataRow($DataType, 'slug');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => "ЧПУ",
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    'slugify' => [
                        'origin' => 'title',
                    ],
                    'validation' => [
                        'rule'  => 'unique:categories,slug',
                    ],
                ],
                'order' => 6,
            ])->save();
        }



    }
}
