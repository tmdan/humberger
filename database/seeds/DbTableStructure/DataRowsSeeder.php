<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Models\DataRow;


class DataRowsSeeder extends Seeder
{



    public function run()
    {
        Model::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('data_rows')->truncate();

        $this->call(UsersDataRowsSeeders::class);
        $this->call(CategoriesDataRowsSeeders::class);
        $this->call(ProductsDataRowsSeeders::class);
        $this->call(MenuDataRowsSeeders::class);

    }









    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
