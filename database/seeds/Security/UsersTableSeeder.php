<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {

        Model::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();


        $admin = Role::where('name', 'admin')->firstOrFail();
        $dev = Role::where('name', 'dev')->firstOrFail();
        $guest = Role::where('name', 'guest')->firstOrFail();

        User::create([
            'name'           => 'Developer',
            'settings'       =>collect(["locale"=>"ru"]),
            'avatar'         => 'users/dev.png',
            'email'          => 'https://t.me/tmdan',
            'password'       => bcrypt(env("DEV_PASSWORD",'@password')),
            'remember_token' => Str::random(60),
            'role_id'        => $dev->id,
        ]);


        User::create([
            'name'           => 'Admin',
            'settings'       =>collect(["locale"=>"ru"]),
            'avatar'         => 'users/admin.png',
            'email'          => 'admin',
            'password'       => bcrypt(env("ADMIN_PASSORD",'password')),
            'remember_token' => Str::random(60),
            'role_id'        => $admin->id,
        ]);


        User::create([
            'name'           => 'Гость',
            'settings'       =>collect(["locale"=>"ru"]),
            'avatar'         => 'users/admin.png',
            'email'          => 'test',
            'password'       => bcrypt('test'),
            'remember_token' => Str::random(60),
            'role_id'        => $guest->id,
        ]);





    }
}
