<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;

class PermissionRoleTableSeeder extends Seeder
{



    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('permission_role')->truncate();


        $admin = Role::where('name', 'admin')->firstOrFail();
        $dev = Role::where('name', 'dev')->firstOrFail();
        $guest = Role::where('name', 'guest')->firstOrFail();


        $adminPermisions = Permission::where('table_name','!=',"roles")
            ->where('table_name','!=','settings')
            //->where('table_name','!=','menus')
            ->where('table_name','!=','dev')
            ->orWhere('table_name',null)
            ->where('key','!=','browse_database')
            ->where('key','!=','browse_bread')
            ->where('key','!=','browse_compass')
        ;

        $guestPermisions = Permission::where('key','NOT LIKE',"add_%")
            ->where('key','NOT LIKE',"delete_%")
            ->where('key','NOT LIKE',"edit_%")
            ->where('table_name','!=',"roles")
            ->where('table_name','!=','settings')
            ->where('table_name','!=','dev')
            ->orWhere('table_name',null)
            ->where('key','!=','browse_database')
            ->where('key','!=','browse_bread')
            ->where('key','!=','browse_compass')
        ;




        $admin->permissions()->sync(
            $adminPermisions->pluck('id')->all()
        );


        $guest->permissions()->sync(
            $guestPermisions->pluck('id')->all()
        );


        $dev->permissions()->sync(
            Permission::all()->pluck('id')->all()
        );
    }
}
