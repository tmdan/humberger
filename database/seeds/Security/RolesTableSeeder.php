<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;



class RolesTableSeeder extends Seeder
{



    public function run()
    {



        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('roles')->truncate();



        $role = Role::firstOrNew(['name' => 'admin']);
        if (!$role->exists) {
            $role->fill([
                    'display_name' => "Админ",
                ])->save();
        }



        $role = Role::firstOrNew(['name' => 'dev']);
        if (!$role->exists) {
            $role->fill([
                'display_name' => 'Разработчик',
            ])->save();
        }


        $role = Role::firstOrNew(['name' => 'guest']);
        if (!$role->exists) {
            $role->fill([
                'display_name' => 'Гость',
            ])->save();
        }


    }
}
