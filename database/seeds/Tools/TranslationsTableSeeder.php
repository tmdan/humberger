<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Category;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\MenuItem;
use TCG\Voyager\Models\Page;
use TCG\Voyager\Models\Translation;

class TranslationsTableSeeder extends Seeder
{



    public function run()
    {
        $this->menusTranslations();
    }





    private function menusTranslations()
    {
        $_tpl = ['menu_items', 'title'];
        $_item = $this->findMenuItem('Dashboard');

        if ($_item->exists) {
            $this->trans('ru', $this->arr($_tpl, $_item->id), 'Панель');
        }


        $_item = $this->findMenuItem("Users");
        if ($_item->exists) {
            $this->trans('ru', $this->arr($_tpl, $_item->id), 'Пользователи');
        }


        $_item = $this->findMenuItem("Roles");
        if ($_item->exists) {
            $this->trans('ru', $this->arr($_tpl, $_item->id), 'Роли');
        }


        $_item = $this->findMenuItem("Tools");
        if ($_item->exists) {
            $this->trans('ru', $this->arr($_tpl, $_item->id), 'Инструменты');
        }



        $_item = $this->findMenuItem("Menu Builder");
        if ($_item->exists) {
            $this->trans('ru', $this->arr($_tpl, $_item->id), "Меню");
        }


        $_item = $this->findMenuItem("Database");
        if ($_item->exists) {
            $this->trans('ru', $this->arr($_tpl, $_item->id), "Базаданных");
        }



        $_item = $this->findMenuItem("Compass");
        if ($_item->exists) {
            $this->trans('ru', $this->arr($_tpl, $_item->id), "Компас");
        }




        $_item = $this->findMenuItem("BREAD");
        if ($_item->exists) {
            $this->trans('ru', $this->arr($_tpl, $_item->id), "Хлебные крошки");
        }




        $_item = $this->findMenuItem("Settings");
        if ($_item->exists) {
            $this->trans('ru', $this->arr($_tpl, $_item->id), "Настройки");
        }

    }




    private function findMenuItem($title)
    {
        return MenuItem::where('title', $title)->firstOrFail();
    }

    private function arr($par, $id)
    {
        return [
            'table_name'  => $par[0],
            'column_name' => $par[1],
            'foreign_key' => $id,
        ];
    }

    private function trans($lang, $keys, $value)
    {
        $_t = Translation::firstOrNew(array_merge($keys, [
            'locale' => $lang,
        ]));

        if (!$_t->exists) {
            $_t->fill(array_merge(
                $keys,
                ['value' => $value]
            ))->save();
        }
    }
}
