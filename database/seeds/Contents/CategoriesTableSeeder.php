<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \GuzzleHttp\json_encode;
use App\Category;


class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('categories')->truncate();



        Category::firstOrCreate([
            'title'=>"Бургеры",
            'order'=>1,
            'icon'=>'fa-align-center',
            'slug'=>"burgers",
        ]);



        Category::firstOrCreate([
            'title'=>"Напитки",
            'order'=>2,
            'icon'=>'fa-bitbucket',
            'slug'=>"drinks",
        ]);


        Category::firstOrCreate([
            'title'=>"Фри",
            'order'=>2,
            'icon'=>'fa-bolt',
            'slug'=>"fries",
        ]);


    }
}
