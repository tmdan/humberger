<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \GuzzleHttp\json_encode;
use App\Product;


class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('products')->truncate();


        Product::firstOrCreate([
            'title' => "BACON BURGER",
            'slug' => "bacon_burger",
            'image' => 'products/example/5.jpg',
            'description' => 'Га́мбургер — вид сэндвича, состоящий из рубленой жареной котлеты, подаваемой внутри разрезанной булки. В дополнение к мясу гамбургер может иметь большое количество разнообразных наполнителей, например: кетчуп и майонез, дольку кабачка, листья салата (Lactuca sativa), маринованный огурец, сырой или жареный лук, помидор.',
            'price' => 2.50,
            'order' => 1,
            'on_sale' => false,
            'status' => true,
        ])->categories()->sync([1]);


        Product::firstOrCreate([
            'title' => "CHEESE SANDWICH",
            'slug' => "cheese_sandwich",
            'image' => 'products/example/6.jpg',
            'description' => 'Га́мбургер — вид сэндвича, состоящий из рубленой жареной котлеты, подаваемой внутри разрезанной булки. В дополнение к мясу гамбургер может иметь большое количество разнообразных наполнителей, например: кетчуп и майонез, дольку кабачка, листья салата (Lactuca sativa), маринованный огурец, сырой или жареный лук, помидор.',
            'price' => 2.70,
            'order' => 2,
            'on_sale' => true,
            'status' => true,
        ])->categories()->sync([1]);


        Product::firstOrCreate([
            'title' => "DELICIOUS SANDWICH",
            'slug' => "delicious_sandwich",
            'image' => 'products/example/7.jpg',
            'description' => 'Га́мбургер — вид сэндвича, состоящий из рубленой жареной котлеты, подаваемой внутри разрезанной булки. В дополнение к мясу гамбургер может иметь большое количество разнообразных наполнителей, например: кетчуп и майонез, дольку кабачка, листья салата (Lactuca sativa), маринованный огурец, сырой или жареный лук, помидор.',
            'price' => 2.90,
            'order' => 3,
            'on_sale' => false,
            'status' => true,
        ])->categories()->sync([1]);


        Product::firstOrCreate([
            'title' => "MORNING SANDWICH",
            'slug' => "morning_sandwich",
            'image' => 'products/example/1.png',
            'description' => 'Га́мбургер — вид сэндвича, состоящий из рубленой жареной котлеты, подаваемой внутри разрезанной булки. В дополнение к мясу гамбургер может иметь большое количество разнообразных наполнителей, например: кетчуп и майонез, дольку кабачка, листья салата (Lactuca sativa), маринованный огурец, сырой или жареный лук, помидор.',
            'price' => 2.30,
            'order' => 4,
            'on_sale' => true,
            'status' => false,
        ])->categories()->sync([1]);


        Product::firstOrCreate([
            'title' => "COLA BOTTLE",
            'slug' => "cola_bottle",
            'image' => 'products/example/cola_bottle.jpg',
            'description' => 'Напиток кола — тип газированных сладких напитков, зачастую содержащих кофеин. Название происходит от орешков кола, изначально использовавшихся производителями напитков в качестве источника кофеина.',
            'price' => 1.30,
            'order' => 5,
            'on_sale' => true,
            'status' => true,
        ])->categories()->sync([2]);


        Product::firstOrCreate([
            'title' => "FRESH LIME",
            'slug' => "fresh_lime",
            'image' => 'products/example/fresh_lime.png',
            'description' => 'Напиток безалкогольный сокосодержащий негазированный Вельта-Пенза Fresh Lime - отзывы',
            'price' => 1.30,
            'order' => 5,
            'on_sale' => false,
            'status' => true,
        ])->categories()->sync([2]);



        Product::firstOrCreate([
            'title' => "Cold Tea",
            'slug' => "cold_tea",
            'image' => 'products/example/cold_tea.jpg',
            'description' => 'Холодный чай — один из видов сервировки чая. Как популярный прохладительный напиток, холодный чай получил широкое распространение в России, странах СНГ и Балтии относительно недавно, после 2000 года. ',
            'price' => 1.40,
            'order' => 6,
            'on_sale' => false,
            'status' => true,
        ])->categories()->sync([2]);


        Product::firstOrCreate([
            'title' => "Black Coffee",
            'slug' => "black_coffee",
            'image' => 'products/example/black_coffee.jpg',
            'description' => 'Блэк латте – кофейный концентрат, который достаточно агрессивно рекламируют как универсальное средство для похудения. Чтобы разобраться в свойствах напитка, внимательно изучим его состав, историю появления и опыт применения на практике.',
            'price' => 1.45,
            'order' => 7,
            'on_sale' => false,
            'status' => true,
        ])->categories()->sync([2]);




        Product::firstOrCreate([
            'title' => "PENE SALMONE",
            'slug' => "pine_salmone",
            'image' => 'products/example/8.jpg',
            'description' => 'Блэк латте – кофейный концентрат, который достаточно агрессивно рекламируют как универсальное средство для похудения. Чтобы разобраться в свойствах напитка, внимательно изучим его состав, историю появления и опыт применения на практике.',
            'price' => 3.45,
            'order' => 8,
            'on_sale' => true,
            'status' => true,
        ])->categories()->sync([3]);




        Product::firstOrCreate([
            'title' => "CHEESE BUTTER",
            'slug' => "cheese_butter",
            'image' => 'products/example/9.jpg',
            'description' => 'Блэк латте – кофейный концентрат, который достаточно агрессивно рекламируют как универсальное средство для похудения. Чтобы разобраться в свойствах напитка, внимательно изучим его состав, историю появления и опыт применения на практике.',
            'price' => 1.00,
            'order' => 9,
            'on_sale' => false,
            'status' => true,
        ])->categories()->sync([3]);





        Product::firstOrCreate([
            'title' => "GENERAL BURGER",
            'slug' => "general_burger",
            'image' => 'products/example/general.jpg',
            'description' => 'Блэк латте – кофейный концентрат, который достаточно агрессивно рекламируют как универсальное средство для похудения. Чтобы разобраться в свойствах напитка, внимательно изучим его состав, историю появления и опыт применения на практике.',
            'price' => 6.00,
            'order' => 10,
            'on_sale' => true,
            'status' => true,
        ])->categories()->sync([3,1]);



    }
}
