<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;
use Illuminate\Support\Facades\DB;

class ClientMenuItemsTableSeeder extends Seeder
{




    public function run()
    {
        $menu = Menu::where('name', 'client')->firstOrFail();


        DB::statement('SET FOREIGN_KEY_CHECKS=0');


        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => "Главная",
            'url'     => '',
            'route'   => null,
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-boat',
                'color'      => "#F4A460",
                'parent_id'  => null,
                'order'      => 1,
            ])->save();
        }


        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => "О нас",
            'url'     => '/#about',
            'route'   => null,
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-boat',
                'color'      => "#F4A460",
                'parent_id'  => null,
                'order'      => 1,
            ])->save();
        }



        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => "Меню",
            'url'     => '/#menu',
            'route'   => null,
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-boat',
                'color'      => "#F4A460",
                'parent_id'  => null,
                'order'      => 1,
            ])->save();
        }




        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => "Скидки",
            'url'     => '/#sale',
            'route'   => null,
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-boat',
                'color'      => "#F4A460",
                'parent_id'  => null,
                'order'      => 1,
            ])->save();
        }


    }
}
