<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Models\DataType;

class DataTypesTableSeeder extends Seeder
{



    public function run()
    {


        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('data_types')->truncate();



        $dataType = $this->dataType('slug', 'users');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'users',
                'display_name_singular' => "пользователя",
                'display_name_plural'   => "Пользователи",
                'icon'                  => 'voyager-person',
                'model_name'            => 'App\\User',
                'policy_name'           => 'App\\Policies\\UserPolicy',
                'controller'            => 'App\\Http\\Controllers\\Voyager\\UserController',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }

        $dataType = $this->dataType('slug', 'menus');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'menus',
                'display_name_singular' => __('voyager::seeders.data_types.menu.singular'),
                'display_name_plural'   => __('voyager::seeders.data_types.menu.plural'),
                'icon'                  => 'voyager-list-add',
                'model_name'            => 'TCG\\Voyager\\Models\\Menu',
                'controller'            => '\\App\Http\\Controllers\\Voyager\\MenuController',
                'policy_name'            => '\\App\\Policies\\MenuPolicy',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }


        $dataType = $this->dataType('slug', 'roles');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'roles',
                'display_name_singular' => __('voyager::seeders.data_types.role.singular'),
                'display_name_plural'   => __('voyager::seeders.data_types.role.plural'),
                'icon'                  => 'voyager-lock',
                'model_name'            => 'TCG\\Voyager\\Models\\Role',
                'controller'            => '',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }



        $dataType = $this->dataType('slug', 'categories');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'categories',
                'display_name_singular' => "категорию",
                'display_name_plural'   => "Категории",
                'icon'                  => 'voyager-list',
                'model_name'            => 'App\\Category',
                'controller'            => null,
                'generate_permissions'  => 1,
                'description'           => null,
            ])->save();
        }


        $dataType = $this->dataType('slug', 'products');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'products',
                'display_name_singular' => "товар",
                'display_name_plural'   => "Товары",
                'icon'                  => 'voyager-hotdog',
                'model_name'            => 'App\\Product',
                'controller'            => null,
                'generate_permissions'  => 1,
                'description'           => null,
            ])->save();
        }

    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }
}
