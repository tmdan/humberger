<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::create('categories', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('title');
                $table->string('icon');
                $table->integer('order')->default(1);
                $table->string('slug')->unique();
            });
        } catch (Exception $e) {
            $this->down();
            throw  new Exception($e->getMessage());
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
