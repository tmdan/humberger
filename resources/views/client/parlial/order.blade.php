<!--/order-now-->
<section class="order-sec pb-5 pt-md-0 pt-5" id="order">
    <div class="container py-lg-3">
        <div class="test-info text-center">
            <h3 class="tittle order">
                <span>Free Delivery With</span>Burger Of the Day</h3>

            <div class="row mt-lg-4 mt-3">
                <div class="col-md-6 order-left-content text-right">
                    <h4>$99</h4>
                </div>
                <div class="col-md-6 order-right-content text-left">
                    <ul class="tic-info list-unstyled">
                        <li>

                            <span class="fa fa-long-arrow-right mr-2"></span> Integer sit amet mattis quam

                        </li>
                        <li>

                            <span class="fa fa-long-arrow-right mr-2"></span> Praesent ullamcorper dui turpis

                        </li>
                        <li>

                            <span class="fa fa-long-arrow-right mr-2"></span> Integer sit amet mattis quam

                        </li>

                    </ul>
                </div>
                <div class="read-more mx-auto text-center">
                    <a href="#contact" class="read-more scroll">Read More </a> </div>
            </div>
        </div>
    </div>
</section>
<!--//order-now-->
