<!-- banner-bottom-wthree -->
<section class="banner-bottom-wthree py-5" id="about">
    <div class="container py-md-3">
        <div class="row banner-grids">
            <div class="col-md-6 content-left-bottom text-left pr-lg-5">
                <h4>{{setting('site.title')}}</h4>
                <p class="mt-2 text-left"><strong class="text-capitalize">{{setting('site.title')}}</strong> американская корпорация, до 2010 года крупнейшая в мире сеть ресторанов быстрого питания, работающая по системе франчайзинга. По итогам 2010 года компания занимает <strong class="text-capitalize"> 2-е место</strong> по количеству ресторанов во всём мире после ресторанной сети Subway. </p>

            </div>
            <div class="col-md-6 content-right-bottom text-left">
                <img src="images/ab1.png" alt="news image" class="img-fluid">
            </div>
        </div>
    </div>
</section>
<!-- //banner-bottom-wthree -->
