<!-- mian-content -->
<div class="main-content" style="background-image: url('{{\TCG\Voyager\Facades\Voyager::image($bg)}}') !important;" id="home">
    <div class="layer">
        <!-- header -->
        <header>
            <div class="container-fluid px-lg-5">
                <!-- nav -->
                <nav class="py-4 d-lg-flex">
                    <div id="logo">
                        <h1> <a href="/"><span class="fa fa-align-center" aria-hidden="true"></span>{{setting('site.title')}}</a></h1>
                    </div>
                    <label for="drop" class="toggle">Menu</label>
                    <input type="checkbox" id="drop" />

                    {!!menu('client', "client.menu.layouts")!!}

            </div>
        </header>
        <!-- //header -->
        <div class="container">
            <!-- /banner -->
            <div class="banner-info-w3layouts text-center">
                <h3>Поесть вкусно - это про нас</h3>
                <div class="read-more">
                    <a href="#about" class="read-more scroll">Подробно </a> </div>
            </div>
            <div class="row order-info">
                <div class="middle mt-3 col-md-6 text-left">
                    <ul class="social mb-4">
                        <li><a href="#"><span class="fa fa-facebook icon_facebook"></span></a></li>
                        <li><a href="#"><span class="fa fa-twitter icon_twitter"></span></a></li>
                        <li><a href="#"><span class="fa fa-google-plus icon_google-plus"></span></a></li>
                        <li><a href="#"><span class="fa fa-linkedin icon_linkedin"></span></a></li>
                        <li><a href="#"><span class="fa fa-dribbble icon_dribbble"></span></a></li>
                    </ul>

                </div>
                <div class="middle-right mt-md-3 col-md-6 text-right">
                    <h6><span class="fa fa-phone"></span> Звони : +375255449622</h6>
                </div>
            </div>
        </div>
        <!-- //banner -->
    </div>
</div>
<!--// mian-content -->
