<!--/mid-sec-->
<section class="mid-sec py-5" id="menu">
    <div class="container-fluid py-lg-5">
        <div class="header pb-lg-3 pb-3 text-center">
            <h3 class="tittle mb-lg-3 mb-3"></h3>
        </div>
        <div class="middile-inner-con">
            <div class="tab-main mx-auto text-center">

                @foreach($categories as $caregory)
                    <input id="tab{{$caregory->id}}" type="radio" name="tabs" {{$caregory->id==1?"checked":null}}>
                    <label for="tab{{$caregory->id}}"><span class="fa {{$caregory->icon}}" aria-hidden="true"></span> {{$caregory->title}}</label>
                @endforeach


                @foreach($categories as $caregory)
                    <section id="content{{$caregory->id}}">
                        <div class="ab-info row">
                            @foreach($caregory->products as $product)
                                <div class="col-md-3 ab-content">
                                    <div class="tab-wrap">
                                        <img src="{{\TCG\Voyager\Facades\Voyager::image($product->image)}}"
                                             alt="{{$product->title}}" class="img-fluid">
                                        <div class="ab-info-con">
                                            <h4>{{$product->title}}</h4>
                                            <p class="price">${{$product->price}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </section>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!--//mid-sec-->
