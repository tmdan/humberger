<!--/ about -->
<section class="services py-5" id="sale">
    <div class="container py-md-5">
        <div class="header pb-lg-3 pb-3 text-center">
            <h3 class="tittle two mb-lg-3 mb-3">Покупай по скидке сегодня</h3>
        </div>
        <div class="row ab-info mt-lg-4">
            @foreach($products as $product)
                <div class="col-lg-3 ab-content">
                    <div class="ab-content-inner">
                        <img src="{{\TCG\Voyager\Facades\Voyager::image($product->image)}}" alt="{{$product->title}}" class="img-fluid">
                        <div class="ab-info-con">
                            <h4>{{$product->title}}</h4>
                            <p>${{$product->price}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<!--// about -->
