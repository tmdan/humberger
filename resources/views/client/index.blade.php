@extends('client.layout')

@section('content')

    @include('client.parlial.main', ['bg'=>$bg])
    @include('client.parlial.about')
    <products></products>
    {{--@include('client.parlial.products', ['categories'=>$categories])--}}
    @includeWhen(count($productsOnSale)>0, 'client.parlial.sale', ['products'=>$productsOnSale])
    {{-- @include('client.parlial.order')--}}
    {{-- @include('client.parlial.gallery')--}}
    {{-- @include('client.parlial.comments')--}}
    {{-- @include('client.parlial.feedback')--}}
    {{-- @include('client.parlial.footer')--}}

@endsection

